# Infinite scrolling prototype
The purpose of this project is to build a prototype application, where a user is able to:
* swipe a message horizontally outside of a screen dismiss it
* see a messages list that is automatically populating itself upon scrolling down
* load a lot of messages (~100k)

## Demo
Demo can be found [here](https://infinite-scroll-proto.appspot.com).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:8080/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build:produ` to create a production quality build.

## Static code analysis
To run ts linter run `npm run check`

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
