import {HammerGestureConfig} from '@angular/platform-browser';
// tslint:disable:no-any
const Hammer = (window as any).Hammer;
// tslint:enable:no-any

export class AppHammerConfig extends HammerGestureConfig {
  overrides = {
    pan: {direction: Hammer.DIRECTION_HORIZONTAL},
    tap: {enable: false},
    rotate: {enable: false},
    press: {enable: false},
    pinch: {enable: false},
    swipe: {direction: Hammer.DIRECTION_HORIZONTAL}
  };
}
