import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {finalize, map} from 'rxjs/operators';
import {RequestsMonitorService} from '../services/requests-monitor/requests-monitor.service';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  constructor(private requestsMonitorService: RequestsMonitorService) {}
  private requestsCount = 0;
  /* tslint:disable:no-any */
  /*
   * <any> is being used even in the official docs:
   * https://angular.io/guide/http
   */
  intercept(req: HttpRequest<any>, next: HttpHandler):
      Observable<HttpEvent<any>> {
    ++this.requestsCount;
    this.requestsMonitorService.update(true);
    return next.handle(req).pipe(finalize(() => {
      --this.requestsCount;
      if (!this.requestsCount) {
        this.requestsMonitorService.update(false);
      }
    }));
  }
  /* tslint:enable:no-any */
}
