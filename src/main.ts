/*
 * Importing hammerjs independent of
 * a device type (desktop, laptop, mobile / tablet).
 * The reason is to care about desktop user
 * experience because a user might use
 * a fully-functional desktop OS on a tablet
 * device (e.g. SurfacePro) with a touchscreen,
 * but without a mouse and without a keyboard.
 * In order to adjust a set of supported gestures,
 * modify respective values in AppHammerConfig.
 */
import 'hammerjs';

import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './components/app/app.module';
import {environment} from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule).catch(
    err => console.error(err));
