import {Author} from '../author/author.interface';

export interface Message {
  content: string;
  updated: string;
  id: number;
  author: Author;
}
