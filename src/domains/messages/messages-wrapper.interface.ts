import {Message} from './message.interface';

export interface MessagesWrapper {
  count: number;
  pageToken: string;
  messages: Message[];
}
