export interface Author {
  name: string;
  photoUrl: string;
}
