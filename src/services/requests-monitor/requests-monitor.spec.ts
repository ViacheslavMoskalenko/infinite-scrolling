import {Subscription} from 'rxjs';

import {RequestsMonitorService} from './requests-monitor.service';

describe('Requests monitor service ->', () => {
  let service: RequestsMonitorService;
  let subscription: Subscription;

  beforeEach(() => service = new RequestsMonitorService());
  afterEach(() => subscription.unsubscribe());

  it('should notify there is no requests at the beginning', () => {
    subscription = service.watch().subscribe(
        (value: boolean) => expect(value).toBe(false));
  });

  it('should notify, when a new request has started', () => {
    service.update(true);
    subscription =
        service.watch().subscribe((value: boolean) => expect(value).toBe(true));
  });

  it('should notify, when there are no pending requests left', () => {
    service.update(true);
    service.update(false);
    subscription = service.watch().subscribe(
        (value: boolean) => expect(value).toBe(false));
  });
});
