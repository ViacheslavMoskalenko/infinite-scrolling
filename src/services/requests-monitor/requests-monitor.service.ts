import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class RequestsMonitorService {
  private monitor$ = new BehaviorSubject<boolean>(false);

  watch(): Observable<boolean> {
    return this.monitor$.asObservable();
  }

  update(isThereAnyPendingRequests: boolean): void {
    this.monitor$.next(isThereAnyPendingRequests);
  }
}
