import {PositionService} from './position.service';

describe('Position service ->', () => {
  let element: HTMLElement;

  function getElementTransform(element: HTMLElement): string|null {
    return element.style.transform;
  }

  beforeEach(() => element = document.createElement('div'));

  it('should apply 3d transformation', () => {
    PositionService.applyElementTransform(element, 10, 20, 30);
    expect(getElementTransform(element))
        .toEqual('translate3d(10px, 20px, 30px)');
  });

  it('should apply 3d transformation even when omitting z coordinate', () => {
    PositionService.applyElementTransform(element, 22, 33);
    expect(getElementTransform(element))
        .toEqual('translate3d(22px, 33px, 0px)');
  });
});
