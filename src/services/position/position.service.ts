export class PositionService {
  private static constructTransform(xShift: number, yShift: number, zShift = 0):
      string {
    return `translate3d(${xShift}px, ${yShift}px, ${zShift}px)`;
  }
  static applyElementTransform(
      element: HTMLElement, xAxis: number, yAxis: number, zAxis?: number) {
    element.style.transform =
        PositionService.constructTransform(xAxis, yAxis, zAxis);
  }
}
