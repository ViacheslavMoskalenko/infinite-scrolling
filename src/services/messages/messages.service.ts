import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {MessagesWrapper} from '../../domains/messages/messages-wrapper.interface';
import {APP_SETTINGS} from '../../settings';

@Injectable()
export class MessagesService {
  constructor(private http: HttpClient) {}

  getChunk(limit = 10, pageToken = ''): Observable<MessagesWrapper> {
    const baseUrl = APP_SETTINGS.MESSAGES.API_URL;
    const path = 'messages';
    const query = `limit=${limit}${pageToken ? `&pageToken=${pageToken}` : ''}`;
    const url = `${baseUrl}/${path}?${query}`;
    return this.http.get(url) as Observable<MessagesWrapper>;
  }
}
