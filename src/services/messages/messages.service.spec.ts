import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';

import {MessagesWrapper} from '../../domains/messages/messages-wrapper.interface';

import {MessagesService} from './messages.service';

describe('Messages service ->', () => {
  let service: MessagesService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule(
        {imports: [HttpClientTestingModule], providers: [MessagesService]});
    service = TestBed.get(MessagesService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should fetch a list of a given size if available', () => {
    const mockMessagesWrapper = {
      count: 2,
      pageToken: 'some token',
      messages: new Array(2)
    };
    service.getChunk(2).subscribe((data: MessagesWrapper) => {
      expect(data.count).toBe(2);
      expect(data.messages.length).toBe(2);
      expect(data.pageToken).toBe('some token');
    });
    const req =
        httpMock.expectOne('https://message-list.appspot.com/messages?limit=2');
    req.flush(mockMessagesWrapper);
  });

  it('should fetch a list of messages without passing a limit', () => {
    const mockMessagesWrapper = {
      count: 10,
      pageToken: 'some token',
      messages: new Array(10)
    };
    service.getChunk().subscribe((data: MessagesWrapper) => {
      expect(data.count).toBe(10);
      expect(data.messages.length).toBe(10);
      expect(data.pageToken).toBe('some token');
    });
    const req = httpMock.expectOne(
        'https://message-list.appspot.com/messages?limit=10');
    req.flush(mockMessagesWrapper);
  });

  it('should fetch a new list of messages, when passing a page token', () => {
    const mockMessagesWrapper = {
      count: 20,
      pageToken: 'some other token',
      messages: new Array(20)
    };
    service.getChunk(20, 'abcd').subscribe((data: MessagesWrapper) => {
      expect();
      expect(data.messages.length).toBe(20);
      expect(data.pageToken).toBe('some other token');
    });
    const url =
        'https://message-list.appspot.com/messages?limit=20&pageToken=abcd';
    const req = httpMock.expectOne(url);
    req.flush(mockMessagesWrapper);
  });
});
