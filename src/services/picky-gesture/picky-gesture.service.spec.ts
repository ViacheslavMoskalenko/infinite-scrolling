import {PickyGestureService} from './picky-gesture.service';

describe('Picky gesture service ->', () => {
  describe('For positive angle: ', () => {
    [170, 175].forEach(angle => {
      it(`should treat drag angle more than 170 degree as horizontal: ${angle}`,
         () => {
           expect(PickyGestureService.isPureHorizontalDrag({angle})).toBe(true);
         });
    });

    [160, 165].forEach(angle => {
      it(`should not treat drag angle less than 170 degree as horizontal: ${
             angle}`,
         () => {
           expect(PickyGestureService.isPureHorizontalDrag({angle}))
               .toBe(false);
         });
    });

    [10, 5].forEach(angle => {
      it(`should treat drag angle lower than 10 degree as horizontal: ${angle}`,
         () => {
           expect(PickyGestureService.isPureHorizontalDrag({angle})).toBe(true);
         });
    });

    [11, 15].forEach(angle => {
      it(`should not treat drag angle greater than 10 degree as horizontal: ${
             angle}`,
         () => {
           expect(PickyGestureService.isPureHorizontalDrag({angle}))
               .toBe(false);
         });
    });
  });

  describe('For negative angle: ', () => {
    [-170, -175].forEach(angle => {
      it(`should treat drag angle lower than -170 degree as horizontal: ${
             angle}`,
         () => {
           expect(PickyGestureService.isPureHorizontalDrag({angle})).toBe(true);
         });
    });

    [-169, -160].forEach(angle => {
      it(`should not treat drag angle greater than -170 degree as horizontal: ${
             angle}`,
         () => {
           expect(PickyGestureService.isPureHorizontalDrag({angle}))
               .toBe(false);
         });
    });

    [-10, -5].forEach(angle => {
      it(`should treat drag angle greater than -10 degree as horizontal: ${
             angle}`,
         () => {
           expect(PickyGestureService.isPureHorizontalDrag({angle})).toBe(true);
         });
    });

    [-11, -15].forEach(angle => {
      it(`should not treat drag angle less than -10 degree as horizontal: ${
             angle}`,
         () => {
           expect(PickyGestureService.isPureHorizontalDrag({angle}))
               .toBe(false);
         });
    });
  });
});
