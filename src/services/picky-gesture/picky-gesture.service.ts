export class PickyGestureService {
  static isPureHorizontalDrag(event: {angle: number}): boolean {
    /*
     * Hammer js has an issue when specifying
     * tap direction as horizontal in the override config.
     * It can treat almost vertical top-left, top-right,
     * bottom-left and bottom-right gestures as left and right
     * respectively.
     * To modify behaviour adjust tolerance angle constant.
     */
    const TOLERANCE_ANGLE = 10;
    const PI_DEG = 180;
    const angle = event.angle;
    if (angle > 0) {
      return angle + TOLERANCE_ANGLE >= PI_DEG || angle <= TOLERANCE_ANGLE;
    }
    return angle <= -(PI_DEG - TOLERANCE_ANGLE) || angle >= -TOLERANCE_ANGLE;
  }
}
