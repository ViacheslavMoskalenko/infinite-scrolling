import {DebugElement} from '@angular/core';
import {Component} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';

import {LoadMoreDirective} from './load-more.directive';

@Component({
  template: `
    <div
      class="parent"
      (loadThresholdReached)="reactOnDataRequest()"
      [loadMore]="{threshold: 60, target: 'self'}"
    >
      <div
        class="child"
      ></div>
    </div>
  `,
  styles: [
    `
      .parent {
        height: 200px;
        overflow-y: auto;
      }
    `,
    `
      .parent .child {
        height: 600px;
      }
    `
  ]
})
class LoadMoreDirectiveSpecComponent {
  private thresholdPassed = false;
  reactOnDataRequest(): void {
    this.thresholdPassed = true;
  }
  resetThresholdData(): void {
    this.thresholdPassed = false;
  }
  isScrollLimitReached(): boolean {
    return this.thresholdPassed;
  }
}

describe('Load more directive ->', () => {
  let fixture: ComponentFixture<LoadMoreDirectiveSpecComponent>;
  let scrollContainerDebugEl: DebugElement;
  let scrollContainerNativeEl: HTMLElement;
  let component: LoadMoreDirectiveSpecComponent;

  function scrollVerticallyTo(
      container: DebugElement, threshold: number): void {
    container.nativeElement.scrollTop =
        container.nativeElement.scrollHeight * threshold / 100;
    container.triggerEventHandler('scroll', null);
    fixture.detectChanges();
  }

  function clearScroll(container: HTMLElement) {
    container.scrollTop = 0;
  }

  beforeEach(() => {
    TestBed.configureTestingModule(
        {declarations: [LoadMoreDirective, LoadMoreDirectiveSpecComponent]});
    fixture = TestBed.createComponent(LoadMoreDirectiveSpecComponent);
    fixture.detectChanges();
    component = fixture.componentInstance;
    scrollContainerDebugEl = fixture.debugElement.query(By.css('.parent'));
    scrollContainerNativeEl = scrollContainerDebugEl.nativeElement;
    clearScroll(scrollContainerNativeEl);
  });

  it('should not reach a scroll limit upon start', () => {
    expect(component.isScrollLimitReached()).toBe(false);
  });

  it('should not reach a scroll limit if use has scrolled not enough', () => {
    scrollVerticallyTo(scrollContainerDebugEl, 45);
    expect(component.isScrollLimitReached()).toBe(false);
  });

  it('should reach a scroll limit after user has scrolled to it', () => {
    scrollVerticallyTo(scrollContainerDebugEl, 70);
    expect(component.isScrollLimitReached()).toBe(true);
  });

  it('should not indicate threshold pass if user scrolls up', () => {
    scrollVerticallyTo(scrollContainerDebugEl, 75);
    expect(component.isScrollLimitReached()).toBe(true);
    component.resetThresholdData();
    scrollVerticallyTo(scrollContainerDebugEl, 65);
    expect(component.isScrollLimitReached()).toBe(false);
  });

  afterEach(() => fixture.destroy());
});
