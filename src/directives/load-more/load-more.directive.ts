import {AfterViewInit, Directive, ElementRef, EventEmitter, HostListener, Input, Output} from '@angular/core';

@Directive({selector: '[loadMore]'})
export class LoadMoreDirective implements AfterViewInit {
  @Input() private loadMore = {threshold: 50};
  @Output() private loadThresholdReached = new EventEmitter<void>();
  private lastSeenScrollTop = 0;
  private getCurrentScrollTop(): number {
    return this.elementRef.nativeElement.scrollTop;
  }
  private isScrollThresholdPassed(): boolean {
    const scrollTop = this.getCurrentScrollTop();
    const scrollHeight = this.elementRef.nativeElement.scrollHeight;
    return scrollTop / scrollHeight >= this.loadMore.threshold;
  }
  private setNormalizeThreshold(): void {
    this.loadMore.threshold =
        (Math.round(Math.abs(this.loadMore.threshold)) % 100) / 100;
  }
  private updateLastScrollTop(): void {
    this.lastSeenScrollTop = this.elementRef.nativeElement.scrollTop;
  }
  private isScrollingDown(): boolean {
    return this.elementRef.nativeElement.scrollTop > this.lastSeenScrollTop;
  }
  @HostListener('scroll')
  private reactOnScroll(): void {
    if (this.isScrollingDown() && this.isScrollThresholdPassed()) {
      this.loadThresholdReached.emit();
      this.updateLastScrollTop();
    }
  }
  constructor(private elementRef: ElementRef) {}
  ngAfterViewInit(): void {
    this.setNormalizeThreshold();
  }
}
