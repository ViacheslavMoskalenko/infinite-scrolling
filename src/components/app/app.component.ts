import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';

import {RequestsMonitorService} from '../../services/requests-monitor/requests-monitor.service';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  requestsArePending = false;
  private reqMonitorSubscription: Subscription = {} as Subscription;
  constructor(private requestMonitorService: RequestsMonitorService) {}
  ngOnInit(): void {
    this.reqMonitorSubscription = this.requestMonitorService.watch().subscribe(
        (status: boolean) => this.requestsArePending = status);
  }
  ngOnDestroy(): void {
    this.reqMonitorSubscription.unsubscribe();
  }
}
