import {ScrollingModule} from '@angular/cdk-experimental/scrolling';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule, HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TimeAgoPipe} from 'time-ago-pipe';

import {AppHammerConfig} from '../../configs/hammerjs.config';
import {LoadMoreDirective} from '../../directives/load-more/load-more.directive';
import {MessagesService} from '../../services/messages/messages.service';
import {RequestsMonitorService} from '../../services/requests-monitor/requests-monitor.service';
import {httpInterceptorProviders} from '../../tools/app.interceptors';
import {AppHeaderComponent} from '../app-header/app-header.component';
import {MessageComponent} from '../message/message.component';
import {MessagesComponent} from '../messages/messages.component';
import {SpinnerComponent} from '../spinner/spinner.component';

import {AppComponent} from './app.component';

@NgModule({
  declarations: [
    AppComponent, MessageComponent, MessagesComponent, AppHeaderComponent,
    TimeAgoPipe, LoadMoreDirective, SpinnerComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, ScrollingModule, BrowserAnimationsModule
  ],
  providers: [
    MessagesService, httpInterceptorProviders, RequestsMonitorService,
    {provide: HAMMER_GESTURE_CONFIG, useClass: AppHammerConfig}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
