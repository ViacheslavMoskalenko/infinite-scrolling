import {Component, OnInit} from '@angular/core';

import {Message} from '../../domains/messages/message.interface';
import {MessagesWrapper} from '../../domains/messages/messages-wrapper.interface';
import {MessagesService} from '../../services/messages/messages.service';
import {APP_SETTINGS} from '../../settings';

@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
})
export class MessagesComponent implements OnInit {
  messages: Message[] = [];
  private lastPageToken = '';
  private isLoadingData = false;
  private refreshLastPageToken(newToken: string) {
    this.lastPageToken = newToken;
  }
  constructor(private readonly messagesService: MessagesService) {}

  loadData(): void {
    /*
     * To save a network bandwidth prevent multiple requests
     * since they'll be the same because of the same page token.
     */
    if (this.isLoadingData) {
      return;
    }
    this.isLoadingData = true;
    this.messagesService
        .getChunk(APP_SETTINGS.MESSAGES.CHUNK_FETCH_SIZE, this.lastPageToken)
        .subscribe(
            (messagesWrapper: MessagesWrapper) => {
              this.messages = [...this.messages, ...messagesWrapper.messages];
              this.logReport();
              /*
               * A pageToken will not be presented in a server response
               * once a list limit is reached.
               * To simulate an infinite list just reset a token
               * effectively starting from the beginning.
               */
              this.refreshLastPageToken(messagesWrapper.pageToken || '');
            },
            (err) => console.error('Unable to fetch messages: ', err),
            () => this.isLoadingData = false);
  }

  logReport(): void {
    if (!APP_SETTINGS.MESSAGES.LOG_LOADED_QUANTITY) {
      return;
    }
    console.log('Messages length: ', this.messages.length);
  }

  ngOnInit(): void {
    this.loadData();
  }

  removeMessage(message: Message): void {
    /*
     * Deletion of an array item takes O(n) (to distribute the remaining
     * elements). To trigger a change detection we have to change a reference to
     * the messages array. To do that just construct a new array of pointers
     * excluding a pointer to a message that has to be removed (takes the same
     * complexity: O(n)).
     * Note that it is possible to have message duplicates since fetch requests
     * start from the beginning at some point.
     * However, only the message a user has swiped out is going to be removed,
     * because it is presented as a unique object.
     * This also brings time-saving benefits because duplicate item won't be
     * compiled by *ngFor and instead the previously created DOM element for a
     * message with the same id is going to be reused.
     */
    this.messages = this.messages.filter((item: Message) => item !== message);
  }

  trackByFn(index: number, message: Message): number {
    return message.id;
  }
}
