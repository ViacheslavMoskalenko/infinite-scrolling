import {animate, keyframes, transition, trigger} from '@angular/animations';
import * as animations from '../../layout/animations';

export const MSG_DISMISS_TO_RIGHT_STATE = 'messageDismissedToRight';
export const MSG_DISMISS_TO_LEFT_STATE = 'messageDismissedToLeft';

export const messageComponentAnimations = [trigger('messageAnimationState', [
  transition(
      `* => ${MSG_DISMISS_TO_RIGHT_STATE}`,
      animate('350ms ease', keyframes(animations.shiftToRightWithFade))),
  transition(
      `* => ${MSG_DISMISS_TO_LEFT_STATE}`,
      animate('350ms ease', keyframes(animations.shiftToLeftWithFade)))
])];
