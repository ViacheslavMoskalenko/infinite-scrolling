import {AnimationEvent} from '@angular/animations';
import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, Output} from '@angular/core';

import {Message} from '../../domains/messages/message.interface';
import {PickyGestureService} from '../../services/picky-gesture/picky-gesture.service';
import {PositionService} from '../../services/position/position.service';
import {APP_SETTINGS} from '../../settings';

import {messageComponentAnimations, MSG_DISMISS_TO_LEFT_STATE, MSG_DISMISS_TO_RIGHT_STATE} from './message.component.animations';

@Component({
  selector: 'message',
  templateUrl: './message.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./message.component.scss'],
  animations: messageComponentAnimations
})
export class MessageComponent implements OnChanges {
  animationState = '';
  authorPicUrl = '';
  private swipeInitiated = false;
  @Input() readonly message: Message = {} as Message;
  @Output() private readonly removeMessage = new EventEmitter<Message>();

  constructor(private readonly elRef: ElementRef) {
    this.resetAnimation();
  }

  private resetAnimation(): void {
    this.animationState = '';
    this.swipeInitiated = false;
    /*
     * If a user swipes an element, there will
     * be a shiftFade animation applied.
     * However, any transformations that were
     * applied during dragging, should be reset.
     * Otherwise, a transformation might be applied
     * to another message element, when iterating (for
     * example, using *ngFor) through a list of elements
     * and using trackBy function to reuse already
     * compiled DOM nodes
     */
    this.resetTransforms();
  }

  private resetTransforms(): void {
    PositionService.applyElementTransform(this.elRef.nativeElement, 0, 0);
  }

  private emitDeleteIntent(): void {
    this.removeMessage.emit(this.message);
  }

  startDrag(event: HammerInput): void {
    if (!PickyGestureService.isPureHorizontalDrag(event)) {
      return;
    }
    PositionService.applyElementTransform(
        this.elRef.nativeElement, event.deltaX, 0);
  }

  stopDrag(): void {
    /*
     * Drag might be stopped while swiping.
     * In that case, we do nothing to prevent strangely
     * looking animation of firstly jumping back
     * to the original position and then moving towards swipe
     * direction behind a viewport.
     * If a user just panned an element a little bit,
     * return it back to the original position.
     */
    if (this.swipeInitiated) {
      return;
    }
    this.resetTransforms();
  }

  applyAnimationState(state: string): void {
    this.animationState = state;
  }

  startAnimation(event: AnimationEvent): void {
    switch (event.toState) {
      case MSG_DISMISS_TO_RIGHT_STATE:
      case MSG_DISMISS_TO_LEFT_STATE:
        this.swipeInitiated = true;
        break;
      default:
        break;
    }
  }

  finishAnimation(event: AnimationEvent): void {
    switch (event.toState) {
      case MSG_DISMISS_TO_RIGHT_STATE:
      case MSG_DISMISS_TO_LEFT_STATE:
        this.emitDeleteIntent();
        break;
      default:
        break;
    }
    this.resetAnimation();
  }

  ngOnChanges() {
    if (this.message.author) {
      this.authorPicUrl =
          `${APP_SETTINGS.MESSAGES.API_URL}/${this.message.author.photoUrl}`;
    }
  }
}
