import {Component} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent {
  activateMenu(): void {
    /*
     * The prototype doesn't assume any functionality
     * encapsulated into the menu.
     * Hence notify user using a standard browser dialog.
     */
    alert(
        'The menu presented as a placeholder only. Sorry for the inconvenience.');
  }
}
