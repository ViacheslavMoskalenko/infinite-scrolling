import {style} from '@angular/animations';

export const shiftToRightWithFade = [
  style({offset: 0, opacity: 1}), style({offset: 0.1, opacity: 0.5}),
  style({offset: 1, transform: 'translate3d(100%, 0, 0)', opacity: 0})
];

export const shiftToLeftWithFade = [
  style({offset: 0, opacity: 1}), style({offset: 0.1, opacity: 0.5}),
  style({offset: 1, transform: 'translate3d(-100%, 0, 0)', opacity: 0})
];
