export const APP_SETTINGS = {
  MESSAGES: {
    CHUNK_FETCH_SIZE: 100,
    API_URL: 'https://message-list.appspot.com',
    LOG_LOADED_QUANTITY: false
  }
};
